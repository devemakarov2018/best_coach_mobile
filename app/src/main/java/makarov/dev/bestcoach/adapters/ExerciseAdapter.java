package makarov.dev.bestcoach.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.ExerciseStudent;

public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ExerciseHolder> {

    static class ExerciseHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView exerciseTitle;
        TextView exerciseDescription;
        TextView repeatsCount;
        Button skipButton;
        Button completeButton;
        ExerciseAdapter.Listener listener;

        ExerciseHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.exercise_item);
            exerciseTitle = itemView.findViewById(R.id.item_title);
            exerciseDescription = itemView.findViewById(R.id.item_description);
            repeatsCount = itemView.findViewById(R.id.repeats_count);
            skipButton = itemView.findViewById(R.id.button_skip);
            completeButton = itemView.findViewById(R.id.button_complete);

            skipButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int exerciseId = (int) skipButton.getTag();
                    listener.onItemSelected(exerciseId, false, true);
                }
            });

            completeButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int exerciseId = (int) skipButton.getTag();
                    listener.onItemSelected(exerciseId, true, false);
                }
            });

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int exerciseId = (int) cardView.getTag();
                    listener.onItemClicked(exerciseId);
                }
            });
        }

        void setListener(ExerciseAdapter.Listener listener) {
            this.listener = listener;
        }
    }

    public interface Listener {
        void onItemSelected(int exerciseId, boolean success, boolean fail);
        void onItemClicked(int exerciseId);
    }

    List<ExerciseStudent> exercisesList;
    Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public ExerciseAdapter(List<ExerciseStudent> exercisesList) {
        this.exercisesList = exercisesList;
    }

    @NonNull
    @Override
    public ExerciseHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.exercise_item, viewGroup, false);
        ExerciseHolder holder = new ExerciseHolder(v);
        holder.setListener(this.listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ExerciseHolder exerciseHolder, int i) {
        exerciseHolder.exerciseTitle.setText(exercisesList.get(i).getExercise().getName());
        exerciseHolder.exerciseDescription.setText(exercisesList.get(i).getExercise().getDescription());
        exerciseHolder.repeatsCount.setText("Всього повторів " + String.valueOf(exercisesList.get(i).getRepeats().size()));
        exerciseHolder.skipButton.setTag(exercisesList.get(i).getExercise_id());
        exerciseHolder.completeButton.setTag(exercisesList.get(i).getExercise_id());
        exerciseHolder.cardView.setTag(exercisesList.get(i).getExercise_id());
    }

    @Override
    public int getItemCount() {
        return exercisesList.size();
    }
}
