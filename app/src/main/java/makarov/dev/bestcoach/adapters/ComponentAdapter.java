package makarov.dev.bestcoach.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.Component;

public class ComponentAdapter extends RecyclerView.Adapter<ComponentAdapter.ComponentHolder> {

    static class ComponentHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView title;
        TextView text;
        ImageView image;
        TextView video;
        TextView link;
        ComponentAdapter.Listener listener;

        ComponentHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.component_item);
            title = itemView.findViewById(R.id.component_title);
            text = itemView.findViewById(R.id.component_text);
            image = itemView.findViewById(R.id.component_image);
            video = itemView.findViewById(R.id.component_video);
            link = itemView.findViewById(R.id.component_link);

            video.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = (String) video.getTag();
                    listener.onVideoClicked(url);
                }
            });

            link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = (String) link.getTag();
                    listener.onLinkClicked(url);
                }
            });
        }

        void setListener(ComponentAdapter.Listener listener) {
            this.listener = listener;
        }
    }

    public interface Listener {
        void onVideoClicked(String url);
        void onLinkClicked(String url);
    }

    List<Component> components;
    ComponentAdapter.Listener listener;

    public void setListener(ComponentAdapter.Listener listener) {
        this.listener = listener;
    }

    public ComponentAdapter(List<Component> components) {
        this.components = components;
    }

    @NonNull
    @Override
    public ComponentHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.component_item, viewGroup, false);
        ComponentHolder holder = new ComponentHolder(v);
        holder.setListener(listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ComponentHolder componentHolder, int i) {

        componentHolder.title.setText(components.get(i).getLabel());
        String componentType = components.get(i).getType();

        switch (componentType) {
            case "text":
                componentHolder.text.setText(components.get(i).getLabel());
                componentHolder.text.setVisibility(View.VISIBLE);
                break;
            case "image":
                Picasso.get().load("http://" + components.get(i).getImage()).into(componentHolder.image);
                componentHolder.image.setVisibility(View.VISIBLE);
                break;
            case "video":
                componentHolder.video.setText("Переглянути відео...");
                componentHolder.video.setTag(components.get(i).getVideo());
                componentHolder.video.setVisibility(View.VISIBLE);
                break;
            case "link":
                componentHolder.link.setText("Переглянути посилання...");
                componentHolder.link.setTag(components.get(i).getLink());
                componentHolder.link.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public int getItemCount() {
        return components.size();
    }
}
