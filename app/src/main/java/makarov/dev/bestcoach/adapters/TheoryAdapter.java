package makarov.dev.bestcoach.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.TheoryDetailStudent;

public class TheoryAdapter extends RecyclerView.Adapter<TheoryAdapter.TheoryHolder> {

    static class TheoryHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView title;
        TextView description;
        Listener listener;

        TheoryHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.theory_item);
            title = itemView.findViewById(R.id.item_title);
            description = itemView.findViewById(R.id.item_description);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int theoryId = (int) cardView.getTag();
                    listener.onItemClicked(theoryId);
                }
            });
        }

        void setListener(Listener listener) {
            this.listener = listener;
        }
    }

    public interface Listener {
        void onItemClicked(int theoryId);
    }

    List<TheoryDetailStudent> theoryList;
    Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public TheoryAdapter(List<TheoryDetailStudent> theoryList) {
        this.theoryList = theoryList;
    }

    @NonNull
    @Override
    public TheoryHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.theory_item, viewGroup, false);
        TheoryHolder holder = new TheoryHolder(v);
        holder.setListener(this.listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull TheoryHolder theoryHolder, int i) {
        theoryHolder.title.setText(theoryList.get(i).getTheory_detail().getName());
        theoryHolder.description.setText(theoryList.get(i).getTheory_detail().getDescription());
        theoryHolder.cardView.setTag(theoryList.get(i).getTheory_detail_id());
    }

    @Override
    public int getItemCount() {
        return theoryList.size();
    }
}
