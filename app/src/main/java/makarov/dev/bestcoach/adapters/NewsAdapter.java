package makarov.dev.bestcoach.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.News;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsHolder> {

    static class NewsHolder extends RecyclerView.ViewHolder {

        CardView cardView;
        TextView title;
        TextView description;
        Listener listener;

        NewsHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.news_item);
            title = itemView.findViewById(R.id.item_title);
            description = itemView.findViewById(R.id.item_description);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int newsId = (int) cardView.getTag();
                    listener.onItemClicked(newsId);
                }
            });
        }

        void setListener(Listener listener) {
            this.listener = listener;
        }
    }

    public interface Listener {
        void onItemClicked(int theoryId);
    }

    List<News> newsList;
    Listener listener;

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public NewsAdapter(List<News> newsList) {
        this.newsList = newsList;
    }

    @NonNull
    @Override
    public NewsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.news_item, viewGroup, false);
        NewsHolder holder = new NewsHolder(v);
        holder.setListener(this.listener);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsHolder newsHolder, int i) {
        newsHolder.title.setText(newsList.get(i).getName());
        newsHolder.description.setText(newsList.get(i).getDescription());
        newsHolder.cardView.setTag(newsList.get(i).getId());
    }

    @Override
    public int getItemCount() {
        return newsList.size();
    }
}
