package makarov.dev.bestcoach.models;

import java.util.List;

public class News {

    private int id;
    private String name;
    private String description;
    private List<Component> news_components;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Component> getNews_components() {
        return news_components;
    }

    public void setNews_components(List<Component> news_components) {
        this.news_components = news_components;
    }
}
