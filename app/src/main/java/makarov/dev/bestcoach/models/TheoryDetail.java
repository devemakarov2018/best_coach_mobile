package makarov.dev.bestcoach.models;

import java.util.List;

public class TheoryDetail {

    private int id;
    private String name;
    private String description;
    private List<Component> theory_details_components;
    private List<Category> theory_details_categories;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Component> getTheory_details_components() {
        return theory_details_components;
    }

    public void setTheory_details_components(List<Component> theory_details_components) {
        this.theory_details_components = theory_details_components;
    }

    public List<Category> getCategories() {
        return theory_details_categories;
    }

    public void setCategories(List<Category> theory_details_categories) {
        this.theory_details_categories = theory_details_categories;
    }
}
