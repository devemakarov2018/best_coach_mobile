package makarov.dev.bestcoach.models;

import java.util.List;

public class Exercise {

    private int id;
    private String name;
    private String description;
    private List<Component> exercise_components;
    private List<Category> exercise_categories;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Component> getExercise_components() {
        return exercise_components;
    }

    public void setExercise_components(List<Component> exercise_components) {
        this.exercise_components = exercise_components;
    }

    public List<Category> getCategories() {
        return exercise_categories;
    }

    public void setCategories(List<Category> exercise_categories) {
        this.exercise_categories = exercise_categories;
    }
}
