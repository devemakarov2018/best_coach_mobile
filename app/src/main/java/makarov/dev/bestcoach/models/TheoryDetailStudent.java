package makarov.dev.bestcoach.models;

public class TheoryDetailStudent {

    private int id;
    private int theory_detail_id;
    private int student_id;
    private TheoryDetail theory_detail;
    private Student student;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTheory_detail_id() {
        return theory_detail_id;
    }

    public void setTheory_detail_id(int theory_detail_id) {
        this.theory_detail_id = theory_detail_id;
    }

    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }

    public TheoryDetail getTheory_detail() {
        return theory_detail;
    }

    public void setTheory_detail(TheoryDetail theory_detail) {
        this.theory_detail = theory_detail;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }
}
