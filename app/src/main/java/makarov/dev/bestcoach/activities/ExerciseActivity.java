package makarov.dev.bestcoach.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.adapters.ComponentAdapter;
import makarov.dev.bestcoach.api.ExercisesService;
import makarov.dev.bestcoach.models.Exercise;
import makarov.dev.bestcoach.models.Category;
import makarov.dev.bestcoach.models.ExerciseStudent;

public class ExerciseActivity extends AppCompatActivity implements ExercisesService.Listener, ComponentAdapter.Listener {

    private RecyclerView componentsList = null;
    private RecyclerView.LayoutManager layoutManager = null;
    private TextView title = null;
    private TextView description = null;
    private TextView categories = null;
    private ExercisesService exercises = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise);

        this.title = findViewById(R.id.exercise_name);
        this.description = findViewById(R.id.exercise_description);
        this.categories = findViewById(R.id.exercise_categories);
        this.componentsList = findViewById(R.id.exercise_components);
        this.layoutManager = new LinearLayoutManager(this);
        this.componentsList.setLayoutManager(this.layoutManager);

        Intent intent = getIntent();
        int exerciseId = intent.getIntExtra("exerciseId", 0);
        this.exercises = new ExercisesService(this);
        this.exercises.setListener(this);
        this.exercises.get(exerciseId);
        setTitle(getString(R.string.loading));
    }

    @Override
    public void onFetchExercisesSuccess(List<ExerciseStudent> exercisesList) {

    }

    @Override
    public void onFetchExercisesError() {

    }

    @Override
    public void onMarkWorkoutSuccess() {

    }

    @Override
    public void onMarkWorkoutError() {

    }

    @Override
    public void onGetSuccess(Exercise exercise) {
        ComponentAdapter adapter = new ComponentAdapter(exercise.getExercise_components());
        componentsList.setAdapter(adapter);
        adapter.setListener(this);
        this.title.setText(exercise.getName());
        this.description.setText(exercise.getDescription());
        StringBuilder stringBuilder = new StringBuilder();
        for (Category category : exercise.getCategories()) {
            stringBuilder
                    .append(category.getName())
                    .append(" ");
        }
        this.categories.setText(stringBuilder.toString());
        setTitle("Опис вправи");
    }

    @Override
    public void onGetError() {
        Toast.makeText(this, "Не вдалося завантажити інформацію про вправу", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onVideoClicked(String url) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception exception) {
            Toast.makeText(this, "Неправильний формат посилання", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onLinkClicked(String url) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception exception) {
            Toast.makeText(this, "Неправильний формат посилання", Toast.LENGTH_LONG).show();
        }
    }
}
