package makarov.dev.bestcoach.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.adapters.ComponentAdapter;
import makarov.dev.bestcoach.api.TheoryService;
import makarov.dev.bestcoach.models.Category;
import makarov.dev.bestcoach.models.TheoryDetail;
import makarov.dev.bestcoach.models.TheoryDetailStudent;

public class TheoryActivity extends AppCompatActivity implements TheoryService.Listener, ComponentAdapter.Listener {

    private RecyclerView componentsList = null;
    private RecyclerView.LayoutManager layoutManager = null;
    private TextView title = null;
    private TextView description = null;
    private TextView categories = null;
    private TheoryService theoryService = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theory);

        this.title = findViewById(R.id.theory_name);
        this.description = findViewById(R.id.theory_description);
        this.categories = findViewById(R.id.theory_categories);
        this.componentsList = findViewById(R.id.theory_components);
        this.layoutManager = new LinearLayoutManager(this);
        this.componentsList.setLayoutManager(this.layoutManager);

        Intent intent = getIntent();
        int theoryDetailId = intent.getIntExtra("theoryDetailId", 0);
        this.theoryService = new TheoryService(this);
        this.theoryService.setListener(this);
        this.theoryService.get(theoryDetailId);
        setTitle(getString(R.string.loading));
    }

    @Override
    public void onVideoClicked(String url) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception exception) {
            Toast.makeText(this, "Неправильний формат посилання", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLinkClicked(String url) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception exception) {
            Toast.makeText(this, "Неправильний формат посилання", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onGetSuccess(TheoryDetail theoryDetail) {
        ComponentAdapter adapter = new ComponentAdapter(theoryDetail.getTheory_details_components());
        componentsList.setAdapter(adapter);
        adapter.setListener(this);
        this.title.setText(theoryDetail.getName());
        this.description.setText(theoryDetail.getDescription());
        StringBuilder stringBuilder = new StringBuilder();
        for (Category category : theoryDetail.getCategories()) {
            stringBuilder
                    .append(category.getName())
                    .append(" ");
        }
        this.categories.setText(stringBuilder.toString());
        setTitle("Теоретичні відомості");
    }

    @Override
    public void onGetError() {

    }

    @Override
    public void onFetchTheoryDetailsSuccess(List<TheoryDetailStudent> theoryDetailList) {

    }

    @Override
    public void onFetchTheoryDetailsError() {

    }
}
