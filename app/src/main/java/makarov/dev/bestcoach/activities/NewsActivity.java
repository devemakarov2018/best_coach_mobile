package makarov.dev.bestcoach.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.adapters.ComponentAdapter;
import makarov.dev.bestcoach.api.NewsService;
import makarov.dev.bestcoach.api.TheoryService;
import makarov.dev.bestcoach.models.Category;
import makarov.dev.bestcoach.models.News;
import makarov.dev.bestcoach.models.TheoryDetail;
import makarov.dev.bestcoach.models.TheoryDetailStudent;

public class NewsActivity extends AppCompatActivity implements NewsService.Listener, ComponentAdapter.Listener {

    private RecyclerView componentsList = null;
    private RecyclerView.LayoutManager layoutManager = null;
    private TextView title = null;
    private TextView description = null;
    private NewsService news = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);

        this.title = findViewById(R.id.news_name);
        this.description = findViewById(R.id.news_description);
        this.componentsList = findViewById(R.id.news_components);
        this.layoutManager = new LinearLayoutManager(this);
        this.componentsList.setLayoutManager(this.layoutManager);

        Intent intent = getIntent();
        int newsId = intent.getIntExtra("newsId", 0);
        this.news = new NewsService(this);
        this.news.setListener(this);
        this.news.get(newsId);
        setTitle(getString(R.string.loading));
    }

    @Override
    public void onVideoClicked(String url) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception exception) {
            Toast.makeText(this, "Неправильний формат посилання", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onLinkClicked(String url) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (Exception exception) {
            Toast.makeText(this, "Неправильний формат посилання", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onGetSuccess(News news) {
        ComponentAdapter adapter = new ComponentAdapter(news.getNews_components());
        componentsList.setAdapter(adapter);
        adapter.setListener(this);
        this.title.setText(news.getName());
        this.description.setText(news.getDescription());
        setTitle("Опис новини");
    }

    @Override
    public void onGetError() {

    }

    @Override
    public void onFetchNewsSuccess(List<News> newsList) {

    }

    @Override
    public void onFetchNewsError() {

    }
}
