package makarov.dev.bestcoach.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.adapters.TheoryAdapter;
import makarov.dev.bestcoach.api.AuthService;
import makarov.dev.bestcoach.api.ScreensService;
import makarov.dev.bestcoach.api.TheoryService;
import makarov.dev.bestcoach.models.Screen;
import makarov.dev.bestcoach.models.Student;
import makarov.dev.bestcoach.models.TheoryDetail;
import makarov.dev.bestcoach.models.TheoryDetailStudent;

public class TheoryListActivity extends AppCompatActivity implements TheoryService.Listener, TheoryAdapter.Listener, ScreensService.Listener {

    private AuthService auth = null;
    private TheoryService theoryDetails = null;
    private ScreensService screens = null;
    private RecyclerView theoryList = null;
    private RecyclerView.LayoutManager layoutManager = null;
    private TextView noTheory = null;
    private Map<String, Screen> screensMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theory_list);
        setTitle(R.string.loading);

        this.noTheory = findViewById(R.id.without_theory);
        this.theoryList = findViewById(R.id.theory_list);
        this.layoutManager = new LinearLayoutManager(this);
        this.theoryList.setLayoutManager(this.layoutManager);

        this.auth = new AuthService(this);
        this.theoryDetails = new TheoryService(this);
        this.theoryDetails.setListener(this);
        this.screens = new ScreensService(this);
        this.screens.setListener(this);

        Student user = this.auth.user();
        this.noTheory.setVisibility(View.INVISIBLE);
        this.theoryDetails.fetchTheoryDetails(user.getId());
        this.screens.fetchScreens();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (screensMap != null) {

            Screen exercises = screensMap.get("exercises");
            Screen theory = screensMap.get("theory");
            Screen standards = screensMap.get("standards");
            Screen news = screensMap.get("news");

            if (exercises != null && theory != null && standards != null && news != null) {

                MenuItem exercisesItem = menu.findItem(R.id.button_my_exercises);
                MenuItem theoryItem = menu.findItem(R.id.button_my_theory);
                MenuItem standardsItem = menu.findItem(R.id.button_my_standards);
                MenuItem newsItem = menu.findItem(R.id.button_my_news);

                exercisesItem.setTitle(exercises.getTitle());

                setTitle(theory.getTitle());
                theoryItem.setTitle(theory.getTitle());
                theoryItem.setVisible(theory.isIs_show());

                standardsItem.setTitle(standards.getTitle());
                standardsItem.setVisible(standards.isIs_show());

                newsItem.setTitle(news.getTitle());
                newsItem.setVisible(news.isIs_show());
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.button_my_exercises:
                startActivity(new Intent(this, ExerciseListActivity.class));
                return true;
            case R.id.button_my_news:
                startActivity(new Intent(this, NewsListActivity.class));
                return true;
            case R.id.button_my_standards:
                startActivity(new Intent(this, StandardsListActivity.class));
                return true;
            case R.id.button_help:
                startActivity(new Intent(this, HelpActivity.class));
                return true;
            case R.id.button_logout:
                this.auth.logout();
                startActivity(new Intent(this, LoginActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onItemClicked(int theoryDetailId) {
        Intent intent = new Intent(this, TheoryActivity.class);
        intent.putExtra("theoryDetailId", theoryDetailId);
        startActivity(intent);
    }

    @Override
    public void onFetchTheoryDetailsSuccess(List<TheoryDetailStudent> theoryDetailList) {
        TheoryAdapter adapter = new TheoryAdapter(theoryDetailList);
        theoryList.setAdapter(adapter);
        adapter.setListener(this);
        if (theoryDetailList.size() == 0) {
            this.noTheory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFetchTheoryDetailsError() {
        Toast.makeText(this, "Не вдалося завантажити теоретичні відомості", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFetchScreensSuccess(List<Screen> screens) {

        screensMap = new HashMap<>();
        for (Screen screen : screens) {
            screensMap.put(screen.getScreen(), screen);
        }

        invalidateOptionsMenu();
    }

    @Override
    public void onFetchScreensError() {
        Toast.makeText(this, "Не вдалося завантажити список екранів", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGetSuccess(TheoryDetail exercise) {

    }

    @Override
    public void onGetError() {

    }
}
