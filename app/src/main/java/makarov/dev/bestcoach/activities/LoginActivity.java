package makarov.dev.bestcoach.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.api.AuthService;
import makarov.dev.bestcoach.models.Grade;
import makarov.dev.bestcoach.models.Student;

public class LoginActivity extends AppCompatActivity implements AuthService.Listener {

    private Spinner gradesSpinner;
    private Spinner studentsSpinner;
    private EditText pinInput;
    private Button loginButton;

    private AuthService auth = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        setTitle(R.string.activity_login__title);
        Log.d("LoginActivity", "onCreate");

        this.studentsSpinner = findViewById(R.id.spinner_student);
        this.loginButton = findViewById(R.id.button_login);
        this.pinInput = findViewById(R.id.edit_text_student_pin);
        this.gradesSpinner = findViewById(R.id.spinner_grade);
        this.gradesSpinner.setAdapter(new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item,
                new String[] {getString(R.string.loading)}));

        this.auth = new AuthService(this);

        if (this.auth.user() != null) {
            startActivity(new Intent(this, ExerciseListActivity.class));
            return;
        }
        this.auth.setListener(this);
        this.auth.fetchGrades();

        this.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Student student = (Student) studentsSpinner.getSelectedItem();
                String pin = pinInput.getText().toString();
                if (student != null && pin.length() == 4) {
                    auth.login(student.getId(), pin);
                }
            }
        });
    }

    @Override
    public void onFetchGradesSuccess(final List<Grade> grades) {

        final Context context = this;
        ArrayAdapter<Grade> adapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                grades
        );

        this.gradesSpinner.setAdapter(adapter);
        this.gradesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Log.d("LoginActivity","onItemSelected");
                auth.fetchStudents(grades.get(position).getId());
                studentsSpinner.setAdapter(new ArrayAdapter<>(context,R.layout.support_simple_spinner_dropdown_item,
                        new String[]{getString(R.string.loading)}));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d("LoginActivity", "onNothingSelected");
            }
        });
    }

    @Override
    public void onFetchGradesError(String message) {
        Toast.makeText(this, "Не вдалося завантажити список класів", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onFetchStudentsSuccess(final List<Student> students) {

        ArrayAdapter<Student> adapter = new ArrayAdapter<>(
                this,
                R.layout.support_simple_spinner_dropdown_item,
                students
        );
        this.studentsSpinner.setAdapter(adapter);
        this.studentsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pinInput.setText("");
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public void onFetchStudentError() {
        Toast.makeText(this, R.string.activity_login__students_error_toast, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onLoginSuccess(Student student) {
        startActivity(new Intent(this, ExerciseListActivity.class));
    }

    @Override
    public void onLoginError() {
        Toast.makeText(this, R.string.activity_login__login_error_toast, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onBackPressed() {
    }
}
