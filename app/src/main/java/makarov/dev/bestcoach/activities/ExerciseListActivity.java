package makarov.dev.bestcoach.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.adapters.ExerciseAdapter;
import makarov.dev.bestcoach.api.AuthService;
import makarov.dev.bestcoach.api.ExercisesService;
import makarov.dev.bestcoach.api.ScreensService;
import makarov.dev.bestcoach.models.Exercise;
import makarov.dev.bestcoach.models.ExerciseStudent;
import makarov.dev.bestcoach.models.Screen;
import makarov.dev.bestcoach.models.Student;

public class ExerciseListActivity extends AppCompatActivity implements ExercisesService.Listener, ExerciseAdapter.Listener, ScreensService.Listener {

    private AuthService auth = null;
    private ExercisesService exercises = null;
    private ScreensService screens = null;
    private RecyclerView exercisesList = null;
    private RecyclerView.LayoutManager layoutManager = null;
    private TextView restDay = null;
    private Map<String, Screen> screensMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise_list);
        setTitle(R.string.loading);

        this.restDay = findViewById(R.id.without_exercises);
        this.exercisesList = findViewById(R.id.exercises_list);
        this.layoutManager = new LinearLayoutManager(this);
        this.exercisesList.setLayoutManager(this.layoutManager);

        this.auth = new AuthService(this);
        this.exercises = new ExercisesService(this);
        this.exercises.setListener(this);
        this.screens = new ScreensService(this);
        this.screens.setListener(this);

        Student user = this.auth.user();
        this.restDay.setVisibility(View.INVISIBLE);
        this.exercises.fetchExercises(user.getId());
        this.screens.fetchScreens();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (screensMap != null) {

            Screen exercises = screensMap.get("exercises");
            Screen theory = screensMap.get("theory");
            Screen standards = screensMap.get("standards");
            Screen news = screensMap.get("news");

            if (exercises != null && theory != null && standards != null && news != null) {

                MenuItem exercisesItem = menu.findItem(R.id.button_my_exercises);
                MenuItem theoryItem = menu.findItem(R.id.button_my_theory);
                MenuItem standardsItem = menu.findItem(R.id.button_my_standards);
                MenuItem newsItem = menu.findItem(R.id.button_my_news);

                exercisesItem.setTitle(exercises.getTitle());
                setTitle(exercises.getTitle());

                theoryItem.setTitle(theory.getTitle());
                theoryItem.setVisible(theory.isIs_show());

                standardsItem.setTitle(standards.getTitle());
                standardsItem.setVisible(standards.isIs_show());

                newsItem.setTitle(news.getTitle());
                newsItem.setVisible(news.isIs_show());
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.button_my_theory:
                startActivity(new Intent(this, TheoryListActivity.class));
                return true;
            case R.id.button_my_news:
                startActivity(new Intent(this, NewsListActivity.class));
                return true;
            case R.id.button_my_standards:
                startActivity(new Intent(this, StandardsListActivity.class));
                return true;
            case R.id.button_help:
                startActivity(new Intent(this, HelpActivity.class));
                return true;
            case R.id.button_logout:
                this.auth.logout();
                startActivity(new Intent(this, LoginActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFetchExercisesSuccess(List<ExerciseStudent> exercises) {
        ExerciseAdapter exerciseAdapter = new ExerciseAdapter(exercises);
        exercisesList.setAdapter(exerciseAdapter);
        exerciseAdapter.setListener(this);
        if (exercises.size() == 0) {
            this.restDay.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onFetchExercisesError() {
        Toast.makeText(this, "Не вдалося завантажити вправи", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemSelected(int exerciseId, boolean success, boolean fail) {
        Student user = this.auth.user();
        this.exercises.markWorkout(user.getId(), exerciseId, success, fail);
    }

    @Override
    public void onMarkWorkoutSuccess() {
        Student user = this.auth.user();
        this.restDay.setVisibility(View.INVISIBLE);
        this.exercises.fetchExercises(user.getId());
        this.screens.fetchScreens();
        setTitle(R.string.loading);
    }

    @Override
    public void onMarkWorkoutError() {
        Toast.makeText(this, "Не вдалося відмітити вправу", Toast.LENGTH_LONG).show();
    }

    @Override
    public void onItemClicked(int exerciseId) {
        Intent intent = new Intent(this, ExerciseActivity.class);
        intent.putExtra("exerciseId", exerciseId);
        startActivity(intent);
    }

    @Override
    public void onGetSuccess(Exercise exercise) {

    }

    @Override
    public void onGetError() {

    }

    @Override
    public void onFetchScreensSuccess(List<Screen> screens) {

        screensMap = new HashMap<>();
        for (Screen screen : screens) {
            screensMap.put(screen.getScreen(), screen);
        }

        invalidateOptionsMenu();
    }

    @Override
    public void onFetchScreensError() {
        Toast.makeText(this, "Не вдалося завантажити список екранів", Toast.LENGTH_LONG).show();
    }
}
