package makarov.dev.bestcoach.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.api.AuthService;
import makarov.dev.bestcoach.api.ScreensService;
import makarov.dev.bestcoach.models.Screen;

public class HelpActivity extends AppCompatActivity implements ScreensService.Listener {

    private AuthService auth = null;
    private ScreensService screens = null;
    private Map<String, Screen> screensMap = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help);
        setTitle("Допомога");

        this.auth = new AuthService(this);
        this.screens = new ScreensService(this);
        this.screens.setListener(this);
        this.screens.fetchScreens();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {

        if (screensMap != null) {

            Screen exercises = screensMap.get("exercises");
            Screen theory = screensMap.get("theory");
            Screen standards = screensMap.get("standards");
            Screen news = screensMap.get("news");

            if (exercises != null && theory != null && standards != null && news != null) {

                MenuItem exercisesItem = menu.findItem(R.id.button_my_exercises);
                MenuItem theoryItem = menu.findItem(R.id.button_my_theory);
                MenuItem standardsItem = menu.findItem(R.id.button_my_standards);
                MenuItem newsItem = menu.findItem(R.id.button_my_news);

                exercisesItem.setTitle(exercises.getTitle());

                theoryItem.setTitle(theory.getTitle());
                theoryItem.setVisible(theory.isIs_show());

                standardsItem.setTitle(standards.getTitle());
                standardsItem.setVisible(standards.isIs_show());

                newsItem.setTitle(news.getTitle());
                newsItem.setVisible(news.isIs_show());
            }
        }

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.button_my_exercises:
                startActivity(new Intent(this, ExerciseListActivity.class));
                return true;
            case R.id.button_my_theory:
                startActivity(new Intent(this, TheoryListActivity.class));
                return true;
            case R.id.button_my_news:
                startActivity(new Intent(this, NewsListActivity.class));
                return true;
            case R.id.button_my_standards:
                startActivity(new Intent(this, StandardsListActivity.class));
                return true;
            case R.id.button_logout:
                this.auth.logout();
                startActivity(new Intent(this, LoginActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onFetchScreensSuccess(List<Screen> screens) {
        screensMap = new HashMap<>();
        for (Screen screen : screens) {
            screensMap.put(screen.getScreen(), screen);
        }

        invalidateOptionsMenu();
    }

    @Override
    public void onFetchScreensError() {
        Toast.makeText(this, "Не вдалося завантажити список екранів", Toast.LENGTH_LONG).show();
    }
}
