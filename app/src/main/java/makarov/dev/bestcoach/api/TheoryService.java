package makarov.dev.bestcoach.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.TheoryDetail;
import makarov.dev.bestcoach.models.TheoryDetailStudent;

public class TheoryService {

    private RequestQueue requestQueue = null;
    private Context context = null;
    private String basicUrl = null;
    private String host = null;
    private String token = null;
    private Listener listener = null;

    public interface Listener {

        void onFetchTheoryDetailsSuccess(List<TheoryDetailStudent> theoryDetailList);
        void onFetchTheoryDetailsError();

        void onGetSuccess(TheoryDetail theoryDetail);
        void onGetError();
    }

    public TheoryService(Context context) {
        this.context = context;
        this.requestQueue = Volley.newRequestQueue(context);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void fetchTheoryDetails(int studentId) {
        this.beforeRequest();
        Log.d("TheoryService", "fetchTheoryDetails");
        try {

            JSONObject body = new JSONObject();
            body.put("student_id", String.valueOf(studentId));
            JsonObjectRequest request = new ApiRequest()
                    .method(Request.Method.POST)
                    .url(this.basicUrl + "/theory-details-students/")
                    .token(this.token)
                    .body(body)
                    .onResponse(new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TheoryService", "fetchTheoryDetails.onResponse");
                            try {
                                String json = response
                                        .getJSONObject("result")
                                        .getJSONArray("theoryDetails")
                                        .toString();
                                Gson gson = new Gson();
                                Type collectionType = new TypeToken<List<TheoryDetailStudent>>(){}.getType();
                                List<TheoryDetailStudent> theoryDetails = gson.fromJson(json, collectionType);
                                if (theoryDetails != null) {
                                    listener.onFetchTheoryDetailsSuccess(theoryDetails);
                                    return;
                                }

                            } catch (Exception exception) {
                                Log.d("TheoryService", exception.getMessage());
                            }
                            listener.onFetchTheoryDetailsError();
                        }
                    })
                    .onError(new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("TheoryService", "fetchTheoryDetails.onErrorResponse");
                            listener.onFetchTheoryDetailsError();
                        }
                    })
                    .build();
            this.requestQueue.add(request);

        } catch (Exception exception) {
            Log.d("TheoryService", exception.getMessage());
            listener.onFetchTheoryDetailsError();
        }
    }

    public void get(int theoryDetailId) {
        this.beforeRequest();
        Log.d("TheoryService", "get");
        JsonObjectRequest request = new ApiRequest()
                .method(Request.Method.GET)
                .url(this.basicUrl + "/theory-details/view/" + String.valueOf(theoryDetailId))
                .token(this.token)
                .onResponse(new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("TheoryService", "get.onResponse");
                        try {
                            String json = response
                                    .getJSONObject("result")
                                    .getJSONObject("theoryDetail")
                                    .toString();
                            Gson gson = new Gson();
                            TheoryDetail theoryDetail = gson.fromJson(json, TheoryDetail.class);
                            if (theoryDetail != null) {
                                listener.onGetSuccess(theoryDetail);
                                return;
                            }

                        } catch (Exception exception) {
                            Log.d("TheoryService", exception.getMessage());
                        }
                        listener.onGetError();
                    }
                })
                .onError(new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("TheoryService", "get.onErrorResponse");
                        listener.onGetError();
                    }
                })
                .build();
        this.requestQueue.add(request);
    }

    private void beforeRequest() {
        Log.d("TheoryService", "beforeRequest");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        this.host = preferences.getString("api_host", this.context.getString(R.string.api_host));
        this.token = preferences.getString("api_token", this.context.getString(R.string.api_token));
        this.basicUrl = "http://" + this.host;
    }
}
