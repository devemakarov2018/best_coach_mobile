package makarov.dev.bestcoach.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.Exercise;
import makarov.dev.bestcoach.models.ExerciseStudent;
import makarov.dev.bestcoach.models.Screen;

public class ScreensService {

    private RequestQueue requestQueue = null;
    private Context context = null;
    private String basicUrl = null;
    private String host = null;
    private String token = null;
    private ScreensService.Listener listener = null;

    public interface Listener {

        void onFetchScreensSuccess(List<Screen> screensList);
        void onFetchScreensError();
    }

    public ScreensService(Context context) {
        this.context = context;
        this.requestQueue = Volley.newRequestQueue(context);
    }

    public void setListener(ScreensService.Listener listener) {
        this.listener = listener;
    }

    public void fetchScreens() {
        this.beforeRequest();
        Log.d("ScreensService", "fetchScreens");

        try {
            JsonObjectRequest request = new ApiRequest()
                    .method(Request.Method.GET)
                    .url(this.basicUrl + "/screens")
                    .token(this.token)
                    .onResponse(new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("ScreensService", "fetchScreens.onResponse");
                            try {
                                String json = response
                                        .getJSONObject("result")
                                        .getJSONArray("screens")
                                        .toString();
                                Gson gson = new Gson();
                                Type collectionType = new TypeToken<List<Screen>>(){}.getType();
                                List<Screen> screens = gson.fromJson(json, collectionType);
                                if (screens != null) {
                                    listener.onFetchScreensSuccess(screens);
                                    return;
                                }
                            } catch (Exception exception) {
                                Log.d("ScreensService", exception.getMessage());
                            }
                            listener.onFetchScreensError();
                        }
                    })
                    .onError(new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("ScreensService", "fetchScreens.onError");
                            listener.onFetchScreensError();
                        }
                    })
                    .build();
            this.requestQueue.add(request);

        } catch (Exception exception) {
            Log.d("ScreensService", exception.getMessage());
            listener.onFetchScreensError();
        }
    }

    private void beforeRequest() {
        Log.d("ScreensService", "beforeRequest");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        this.host = preferences.getString("api_host", this.context.getString(R.string.api_host));
        this.token = preferences.getString("api_token", this.context.getString(R.string.api_token));
        this.basicUrl = "http://" + this.host;
    }

}
