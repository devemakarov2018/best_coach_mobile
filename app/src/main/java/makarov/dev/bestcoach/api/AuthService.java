package makarov.dev.bestcoach.api;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.Grade;
import makarov.dev.bestcoach.models.Student;

public class AuthService {

    private RequestQueue requestQueue = null;
    private Context context = null;
    private String basicUrl = null;
    private String host = null;
    private String token = null;

    public interface Listener {

        void onFetchGradesSuccess(List<Grade> grades);
        void onFetchGradesError(String error);

        void onFetchStudentsSuccess(List<Student> students);
        void onFetchStudentError();

        void onLoginSuccess(Student student);
        void onLoginError();
    }

    private Listener listener = null;

    public AuthService(Context context) {
        this.context = context;
        this.requestQueue = Volley.newRequestQueue(context);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void fetchGrades() {
        this.beforeRequest();
        Log.d("AuthService", "fetchGrades");
        JsonObjectRequest request = new ApiRequest()
                .method(Request.Method.GET)
                .url(this.basicUrl + "/grades")
                .token(this.token)
                .onResponse(new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("AuthService", "fetchGrades.onResponse");
                        try {
                            String json = response
                                    .getJSONObject("result")
                                    .getJSONArray("grades")
                                    .toString();
                            Gson gson = new Gson();

                            Type collectionType = new TypeToken<List<Grade>>(){}.getType();
                            List<Grade> grades = gson.fromJson(json, collectionType);
                            listener.onFetchGradesSuccess(grades);

                        } catch (Exception exception) {
                            Log.d("AuthService", exception.getMessage());
                            listener.onFetchGradesError(exception.getMessage());
                        }

                    }
                })
                .onError(new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("AuthService", "fetchGrades.onErrorResponse");
                        listener.onFetchGradesError(error.getMessage());
                    }
                })
                .build();
        this.requestQueue.add(request);
    }

    public void fetchStudents(int gradeId) {

        this.beforeRequest();
        Log.d("AuthService", "fetchStudents");
        JsonObjectRequest request = new ApiRequest()
                .method(Request.Method.GET)
                .url(this.basicUrl + "/students/?grade_id=" + String.valueOf(gradeId))
                .token(this.token)
                .onResponse(new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("AuthService", "fetchStudents.onResponse");
                        try {
                            String json = response
                                    .getJSONObject("result")
                                    .getJSONArray("students")
                                    .toString();
                            Gson gson = new Gson();
                            Type collectionType = new TypeToken<List<Student>>(){}.getType();
                            List<Student> students = gson.fromJson(json, collectionType);
                            if (students.size() > 0) {
                                listener.onFetchStudentsSuccess(students);
                                return;
                            }
                        } catch (Exception exception) {
                            Log.d("AuthService", exception.getMessage());
                        }
                        listener.onFetchStudentError();
                    }
                })
                .onError(new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("AuthService", "fetchStudents.onErrorResponse");
                        listener.onFetchStudentError();
                    }
                })
                .build();
        this.requestQueue.add(request);
    }

    public void login(int studentId, String pin) {

        this.beforeRequest();
        Log.d("AuthService", "login");

        try {
            JSONObject body = new JSONObject();
            body.put("id", String.valueOf(studentId));
            body.put("pin", pin);
            final JsonObjectRequest request = new ApiRequest()
                    .method(Request.Method.POST)
                    .url(this.basicUrl + "/auth/login-student")
                    .body(body)
                    .onResponse(new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("AuthService", "login.onResponse");
                            try {

                                String token = response
                                        .getJSONObject("result")
                                        .getString("token");

                                String json = response
                                        .getJSONObject("result")
                                        .getJSONObject("user")
                                        .toString();
                                save(json, token);

                                Gson gson = new Gson();
                                Student student = gson.fromJson(json, Student.class);
                                listener.onLoginSuccess(student);

                            } catch (Exception exception) {
                                Log.d("AuthService", exception.getMessage());
                                listener.onLoginError();
                            }
                        }
                    })
                    .onError(new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("AuthService", "login.onErrorResponse");
                            listener.onLoginError();
                        }
                    })
                    .build();
            this.requestQueue.add(request);

        } catch (Exception exception) {
            Log.d("AuthService", exception.getMessage());
            listener.onLoginError();
        }
    }

    private void beforeRequest() {
        Log.d("AuthService", "beforeRequest");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        this.host = preferences.getString("api_host", this.context.getString(R.string.api_host));
        this.token = preferences.getString("api_token", this.context.getString(R.string.api_token));
        this.basicUrl = "http://" + this.host;
    }

    private void save(String user, String token) {
        Log.d("AuthService", "login");
        PreferenceManager.getDefaultSharedPreferences(this.context)
                .edit()
                .putString("api_token", token)
                .putString("user", user)
                .apply();
    }

    public Student user() {
        Log.d("AuthService", "user");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        String json = preferences.getString("user", "");
        Gson gson = new Gson();
        return gson.fromJson(json, Student.class);
    }

    public void logout() {
        Log.d("AuthService", "logout");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        preferences.edit()
                .remove("user")
                .apply();
    }
}
