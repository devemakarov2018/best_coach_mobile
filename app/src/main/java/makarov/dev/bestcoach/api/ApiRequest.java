package makarov.dev.bestcoach.api;

import android.support.annotation.Nullable;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

class ApiRequest {

    private int method = 0;
    private String url = null;
    private String token = null;
    private JSONObject body = null;
    private Response.Listener<JSONObject> response = null;
    private Response.ErrorListener error = null;

    ApiRequest() {

    }

    ApiRequest method(int method) {
        Log.d("ApiRequest", "method");
        this.method = method;
        return this;
    }

    ApiRequest url(String url) {
        Log.d("ApiRequest", "url");
        this.url = url;
        return this;
    }

    ApiRequest token(String token) {
        Log.d("ApiRequest", "token");
        this.token = token;
        return this;
    }

    ApiRequest body(@Nullable JSONObject body) {
        Log.d("ApiRequest", "body");
        this.body = body;
        return this;
    }

    ApiRequest onResponse(Response.Listener<JSONObject> response) {
        Log.d("ApiRequest", "onResponse");
        this.response = response;
        return this;
    }

    ApiRequest onError(Response.ErrorListener error) {
        Log.d("ApiRequest", "onError");
        this.error = error;
        return this;
    }

    JsonObjectRequest build() {
        Log.d("ApiRequest", "build");
        return new JsonObjectRequest(this.method, this.url, this.body, this.response, this.error) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Log.d("ApiRequest", "getHeaders");
                Map<String, String> headers = new HashMap <>();
                if (token != null) {
                    headers.put("Authorization", "Bearer " + token);
                }
                return headers;
            }
        };
    }
}
