package makarov.dev.bestcoach.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.Exercise;
import makarov.dev.bestcoach.models.ExerciseStudent;
import makarov.dev.bestcoach.models.Grade;
import makarov.dev.bestcoach.models.Student;

public class ExercisesService {

    private RequestQueue requestQueue = null;
    private Context context = null;
    private String basicUrl = null;
    private String host = null;
    private String token = null;
    private Listener listener = null;

    public interface Listener {

        void onFetchExercisesSuccess(List<ExerciseStudent> exercisesList);
        void onFetchExercisesError();

        void onMarkWorkoutSuccess();
        void onMarkWorkoutError();

        void onGetSuccess(Exercise exercise);
        void onGetError();
    }

    public ExercisesService(Context context) {
        this.context = context;
        this.requestQueue = Volley.newRequestQueue(context);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void fetchExercises(int studentId) {
        this.beforeRequest();
        Log.d("ExercisesService", "fetchExercises");
        try {

            JSONObject body = new JSONObject();
            body.put("student_id", String.valueOf(studentId));
            JsonObjectRequest request = new ApiRequest()
                    .method(Request.Method.POST)
                    .url(this.basicUrl + "/exercises-students/schedule")
                    .token(this.token)
                    .body(body)
                    .onResponse(new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("ExercisesService", "fetchExercises.onResponse");
                            try {
                                String json = response
                                        .getJSONObject("result")
                                        .getJSONArray("exercises")
                                        .toString();
                                Gson gson = new Gson();
                                Type collectionType = new TypeToken<List<ExerciseStudent>>(){}.getType();
                                List<ExerciseStudent> exercises = gson.fromJson(json, collectionType);
                                if (exercises != null) {
                                    listener.onFetchExercisesSuccess(exercises);
                                    return;
                                }

                            } catch (Exception exception) {
                                Log.d("ExercisesService", exception.getMessage());
                            }
                            listener.onFetchExercisesError();
                        }
                    })
                    .onError(new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("ExercisesService", "fetchExercises.onErrorResponse");
                            listener.onFetchExercisesError();
                        }
                    })
                    .build();
            this.requestQueue.add(request);

        } catch (Exception exception) {
            Log.d("ExercisesService", exception.getMessage());
            listener.onFetchExercisesError();
        }
    }

    public void markWorkout(int studentId, int exerciseId, boolean success, boolean fail) {

        this.beforeRequest();
        Log.d("ExercisesService", "markWorkout");
        try {
            JSONObject body = new JSONObject();
            body.put("student_id", studentId);
            body.put("exercise_id", exerciseId);
            JSONObject repeat = new JSONObject();
            repeat.put("success", success);
            repeat.put("fail", fail);
            repeat.put("date", new SimpleDateFormat("yyyy-MM-dd").format(new Date()));
            body.put("repeat", repeat);
            JsonObjectRequest request = new ApiRequest()
                    .method(Request.Method.POST)
                    .url(this.basicUrl + "/exercises-students/mark-workout")
                    .token(this.token)
                    .body(body)
                    .onResponse(new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("ExercisesService", "markWorkout.onResponse");
                            listener.onMarkWorkoutSuccess();
                        }
                    })
                    .onError(new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("ExercisesService", "markWorkout.onErrorResponse");
                            listener.onMarkWorkoutError();
                        }
                    })
                    .build();
            this.requestQueue.add(request);

        } catch (Exception exception) {
            Log.d("ExercisesService", exception.getMessage());
            listener.onMarkWorkoutError();
        }
    }

    public void get(int exerciseId) {
        this.beforeRequest();
        Log.d("ExercisesService", "get");
        JsonObjectRequest request = new ApiRequest()
                .method(Request.Method.GET)
                .url(this.basicUrl + "/exercises/view/" + String.valueOf(exerciseId))
                .token(this.token)
                .onResponse(new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("ExercisesService", "get.onResponse");
                        try {
                            String json = response
                                    .getJSONObject("result")
                                    .getJSONObject("exercise")
                                    .toString();
                            Gson gson = new Gson();
                            Exercise exercise = gson.fromJson(json, Exercise.class);
                            if (exercise != null) {
                                listener.onGetSuccess(exercise);
                                return;
                            }

                        } catch (Exception exception) {
                            Log.d("ExercisesService", exception.getMessage());
                        }
                        listener.onGetError();
                    }
                })
                .onError(new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("ExercisesService", "get.onErrorResponse");
                        listener.onGetError();
                    }
                })
                .build();
        this.requestQueue.add(request);
    }

    private void beforeRequest() {
        Log.d("ExercisesService", "beforeRequest");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        this.host = preferences.getString("api_host", this.context.getString(R.string.api_host));
        this.token = preferences.getString("api_token", this.context.getString(R.string.api_token));
        this.basicUrl = "http://" + this.host;
    }
}
