package makarov.dev.bestcoach.api;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.List;

import makarov.dev.bestcoach.R;
import makarov.dev.bestcoach.models.News;
import makarov.dev.bestcoach.models.TheoryDetail;
import makarov.dev.bestcoach.models.TheoryDetailStudent;

public class NewsService {

    private RequestQueue requestQueue = null;
    private Context context = null;
    private String basicUrl = null;
    private String host = null;
    private String token = null;
    private Listener listener = null;

    public interface Listener {

        void onFetchNewsSuccess(List<News> newsList);
        void onFetchNewsError();

        void onGetSuccess(News news);
        void onGetError();
    }

    public NewsService(Context context) {
        this.context = context;
        this.requestQueue = Volley.newRequestQueue(context);
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public void fetchNews() {
        this.beforeRequest();
        Log.d("NewsService", "fetchNews");
        JsonObjectRequest request = new ApiRequest()
                .method(Request.Method.GET)
                .url(this.basicUrl + "/news")
                .token(this.token)
                .onResponse(new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("NewsService", "fetchNews.onResponse");
                        try {
                            String json = response
                                    .getJSONObject("result")
                                    .getJSONArray("news")
                                    .toString();
                            Gson gson = new Gson();
                            Type collectionType = new TypeToken<List<News>>(){}.getType();
                            List<News> news = gson.fromJson(json, collectionType);
                            if (news  != null) {
                                listener.onFetchNewsSuccess(news);
                                return;
                            }
                        } catch (Exception exception) {
                            Log.d("NewsService", exception.getMessage());
                        }
                        listener.onFetchNewsError();
                    }
                })
                .onError(new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("NewsService", "get.onErrorResponse");
                        listener.onFetchNewsError();
                    }
                })
                .build();
        this.requestQueue.add(request);
    }

    public void get(int newsId) {
        this.beforeRequest();
        Log.d("NewsService", "get");
        JsonObjectRequest request = new ApiRequest()
                .method(Request.Method.GET)
                .url(this.basicUrl + "/news/view/" + String.valueOf(newsId))
                .token(this.token)
                .onResponse(new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.d("NewsService", "get.onResponse");
                        try {
                            String json = response
                                    .getJSONObject("result")
                                    .getJSONObject("news")
                                    .toString();
                            Gson gson = new Gson();
                            News news = gson.fromJson(json, News.class);
                            if (news != null) {
                                listener.onGetSuccess(news);
                                return;
                            }

                        } catch (Exception exception) {
                            Log.d("NewsService", exception.getMessage());
                        }
                        listener.onGetError();
                    }
                })
                .onError(new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("NewsService", "get.onErrorResponse");
                        listener.onGetError();
                    }
                })
                .build();
        this.requestQueue.add(request);
    }

    private void beforeRequest() {
        Log.d("TheoryService", "beforeRequest");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this.context);
        this.host = preferences.getString("api_host", this.context.getString(R.string.api_host));
        this.token = preferences.getString("api_token", this.context.getString(R.string.api_token));
        this.basicUrl = "http://" + this.host;
    }
}
